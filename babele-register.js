Hooks.once('init', () => {
	if(typeof Babele !== 'undefined') {
		Babele.get().register({
			module: 'dnd5e-fr-TaleScape',
			lang: 'fr',
			dir: 'compendium'
		});
	}
});
